using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;

[CreateAssetMenu(fileName = "MyScriptableObject", menuName = "Temp")]
public class MyScriptableObject : ScriptableObject
{
    public int index;
    public string text;

    public void SetPdfContents(string contents)
    {
        text = contents;
    }
}
