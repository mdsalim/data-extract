using UnityEngine;
using System.Xml;

public static class XMLParser
{
    //public static MyScriptableObject ParseXML(string xmlPath)
    //{
    //    MyScriptableObject newData = ScriptableObject.CreateInstance<MyScriptableObject>();

    //    XmlDocument xmlDoc = new XmlDocument();
    //    xmlDoc.Load(Application.dataPath + xmlPath);
    //    Debug.Log(xmlDoc.InnerText);
    //    // Extract data from XML and assign it to the fields of the ScriptableObject
    //    newData.index = int.Parse(xmlDoc.SelectSingleNode("MyScriptableObject/index").InnerText);
    //    newData.text = xmlDoc.SelectSingleNode("MyScriptableObject/text").InnerText;
    //    // Assign more fields as needed

    //    return newData;
    //}
    //////////////////
    public static MyScriptableObject ParseXML(string xmlFilePath)
    {
        // Implement XML parsing logic here to extract the values for 'index' and 'text'
        // Instantiate a new MyScriptableObject instance and assign the parsed values
        // Return the populated MyScriptableObject instance

        // Example implementation:
        MyScriptableObject scriptableObject = ScriptableObject.CreateInstance<MyScriptableObject>();

        // Read XML and extract values for 'index' and 'text'
        // Assume the XML structure contains elements <index> and <text>
        // You may need to modify this code to match your specific XML structure
        var xmlDoc = new System.Xml.XmlDocument();
        xmlDoc.Load(Application.dataPath + xmlFilePath);

        var indexNode = xmlDoc.SelectSingleNode("MyScriptableObject/index");
        if (indexNode != null)
        {
            int indexValue;
            if (int.TryParse(indexNode.InnerText, out indexValue))
            {
                scriptableObject.index = indexValue;
            }
        }

        var textNode = xmlDoc.SelectSingleNode("MyScriptableObject/text");
        if (textNode != null)
        {
            scriptableObject.text = textNode.InnerText;
        }

        return scriptableObject;
    }
}