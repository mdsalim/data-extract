using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;

[CustomEditor(typeof(NewContainer))]
public class NewPdf : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        NewContainer myObject = (NewContainer)target;

        if (GUILayout.Button("Convert to Pdf"))
        {
            GeneratePDF(myObject.myScriptableObject);
        }
        if (GUILayout.Button("Pdf To ScriptableObject"))
        {
            NewPdfToScriptableObject("/Pdf.pdf");
        }
    }
    public void GeneratePDF(MyScriptableObject dataObject)
    {
        // PDF generation code goes here
        PdfDocument document = new PdfDocument();
        XFont font = new XFont("Arial", 12, XFontStyle.Regular);
        XGraphics gfx = XGraphics.FromPdfPage(document.AddPage());
        XTextFormatter tf = new XTextFormatter(gfx);
        
        tf.DrawString(dataObject.index.ToString(), font, XBrushes.Black, new XRect(10, 10, 200, 20));
        tf.DrawString("\n" + dataObject.text, font, XBrushes.Black, new XRect(10, 10, 200, 20));

        document.Save(Application.dataPath + "/Pdf.pdf");
        document.Close();
    }

    private void NewPdfToScriptableObject(string pdfFilePath)
    {
        MyScriptableObject importedData = PDFParser.ParserPDF(pdfFilePath);
    }
}
