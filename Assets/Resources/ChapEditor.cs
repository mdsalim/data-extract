using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Content;

[CustomEditor(typeof(ChapDataGenerator))]
public class ChapEditor : Editor
{
    private const string XmlFileName = "/myObjecter.xml";
    private const string PdfFileName = "/MyScritableObjecter.pdf";

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ChapDataGenerator myObject = (ChapDataGenerator)target;

        if (GUILayout.Button("XML to ScriptableObject"))
        {
            string xmlFilePath = Path.Combine(Application.dataPath, XmlFileName);
            XmlToScriptableObject(xmlFilePath);
        }

        if (GUILayout.Button("Pdf To ScriptableObject"))
        {
            string pdfFilePath = Path.Combine(Application.dataPath, PdfFileName);
            //PdfToScriptableObject(pdfFilePath);
            NewPdfToScriptableObject(PdfFileName);
        }
    }

    private void XmlToScriptableObject(string xmlFilePath)
    {
        //if (!File.Exists(xmlFilePath))
        //{
        //    Debug.Log("XML file does not exist: " + xmlFilePath);
        //    return;
        //}

        MyScriptableObject importedData = XMLParser.ParseXML(xmlFilePath);

        string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(xmlFilePath) + ".asset");
        AssetDatabase.CreateAsset(importedData, assetPath);

        Debug.Log("XML data imported successfully!");

        string pdfFilePath = Path.Combine(Application.dataPath, PdfFileName);
        ConvertScriptableObjectToPdf(importedData, pdfFilePath);

        Debug.Log("XML to ScriptableObject converted to PDF and saved as " + pdfFilePath);
    }
    private void NewPdfToScriptableObject(string pdfFilePath)
    {
        MyScriptableObject importedData = PDFParser.ParserPDF(pdfFilePath);
        //string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(pdfFilePath) + ".asset");
        //AssetDatabase.CreateAsset(importedData, assetPath);
        //Debug.Log("PDF data imported successfully!");
    }
    private void ConvertScriptableObjectToPdf(MyScriptableObject scriptableObject, string pdfFilePath)
    {
        PdfDocument document = new PdfDocument();
        PdfPage page = document.AddPage();

        XGraphics gfx = XGraphics.FromPdfPage(page);
        XFont textFont = new XFont("arial", 6, XFontStyle.Regular);
        XTextFormatter textFormatter = new XTextFormatter(gfx);

        string scriptableObjectContent = GetScriptableObjectContent(scriptableObject);
        textFormatter.DrawString(scriptableObjectContent, textFont, XBrushes.Black, new XRect(15, 15, page.Width - 20, page.Height - 20), XStringFormats.TopLeft);

        document.Save(pdfFilePath);
        document.Close();
    }
    
    private string GetScriptableObjectContent(MyScriptableObject scriptableObject)
    {
        StringBuilder sb = new StringBuilder();

        // Append scriptableObject properties or data to the StringBuilder
        sb.AppendLine("Index: " + scriptableObject.index);
        sb.AppendLine("Text: " + scriptableObject.text);
        
        Debug.LogWarning(sb.ToString());
        return sb.ToString();
    }

    private void PdfToScriptableObject(string pdfFilePath)
    {
        if (!File.Exists(pdfFilePath))
        {
            Debug.LogError("PDF file does not exist: " + pdfFilePath);
            return;
        }

        PdfDocument document = PdfReader.Open(pdfFilePath, PdfDocumentOpenMode.ReadOnly);
        string pdfContents = ExtractPdfContents(document);

        MyScriptableObject scriptableObject = ScriptableObject.CreateInstance<MyScriptableObject>();
        scriptableObject.SetPdfContents(pdfContents);

        string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(pdfFilePath) + ".asset");
        AssetDatabase.CreateAsset(scriptableObject, assetPath);

        Debug.Log("PDF data imported successfully!");
    }

    private string ExtractPdfContents(PdfDocument document)
    {
        StringBuilder sb = new StringBuilder();

        foreach (PdfPage page in document.Pages)
        {
            var content = ContentReader.ReadContent(page);
            sb.Append(content);
        }

        return sb.ToString();
    }


}