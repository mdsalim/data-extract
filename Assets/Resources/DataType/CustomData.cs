using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomData
{
    public string name;
    public int value;
    public VariableType type;
    public object variableValues;
}
public enum VariableType
{
    Int,
    Float,
    String,
    Char
}

