using UnityEngine;

[CreateAssetMenu(fileName = "CustomVVD", menuName = "Variable")]
public class CustomVVD : ScriptableObject
{
    public VariableType variableType;
    public object variableValue;
}
