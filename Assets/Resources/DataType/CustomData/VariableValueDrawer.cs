using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(CustomVVD))]
public class VariableValueDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        SerializedProperty variableType = property.FindPropertyRelative("variableType");
        SerializedProperty variableValue = property.FindPropertyRelative("variableValue");

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        Rect typeRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        Rect valueRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing,
            position.width, EditorGUIUtility.singleLineHeight);

        EditorGUI.PropertyField(typeRect, variableType);

        switch ((VariableType)variableType.enumValueIndex)
        {
            case VariableType.Int:
                EditorGUI.PropertyField(valueRect, variableValue.FindPropertyRelative("intValue"), GUIContent.none);
                break;
            case VariableType.Float:
                EditorGUI.PropertyField(valueRect, variableValue.FindPropertyRelative("floatValue"), GUIContent.none);
                break;
            case VariableType.String:
                EditorGUI.PropertyField(valueRect, variableValue.FindPropertyRelative("stringValue"), GUIContent.none);
                break;
            case VariableType.Char:
                EditorGUI.PropertyField(valueRect, variableValue.FindPropertyRelative("charValue"), GUIContent.none);
                break;
        }

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
    }
}
