using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(DataType_ScriptableObject))]
public class CustomDataTypeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        // Draw the label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Indent the content
        EditorGUI.indentLevel++;

        // Calculate the height of each property field
        float lineHeight = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        Rect nameRect = new Rect(position.x, position.y, position.width, lineHeight);
        Rect valueRect = new Rect(position.x, position.y + lineHeight, position.width, lineHeight);

        // Draw the property fields
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"));
        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("value"));

        // Reset the indent level
        EditorGUI.indentLevel--;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        // Calculate the total height of the property fields
        float lineHeight = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        return lineHeight * 2;
    }
}
