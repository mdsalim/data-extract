using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataType_ScriptableObject", menuName = "CustomData")]
public class DataType_ScriptableObject : ScriptableObject
{
    public CustomData customData;
}
