using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using UnityEngine;

public static class PDFParser 
{
    public static MyScriptableObject ParserPDF(string PdfFilePath)
    {
        PdfDocument document = PdfReader.Open(Application.dataPath + PdfFilePath, PdfDocumentOpenMode.Import);

        //PdfPage page = document.Pages[0]; // Assuming the data is on the first page
        //XGraphics gfx = XGraphics.FromPdfPage(page);
        //XTextFormatter tf = new XTextFormatter(gfx);
        //XRect rect = new XRect(10, 10, 200, 20); // Adjust the coordinates and size according to the text position in the PDF

        //string extractedValue = tf.Format(rect, page); // Extract the text value

        //MyScriptableObject scriptableObject = ScriptableObject.CreateInstance<MyScriptableObject>();
        //scriptableObject.text = extractedValue;

        //document.Close();

        //return scriptableObject;


        MyScriptableObject pdfData = ScriptableObject.CreateInstance<MyScriptableObject>();

        foreach (PdfPage page in document.Pages)
        {
            // Extract text content from the page
            string pageContent = page.Contents.ToString();
            Debug.LogWarning(pageContent);
            // Process the extracted content as needed
            // For example, split the content into lines and extract specific data

            string[] lines = pageContent.Split('\n');
            foreach (string line in lines)
            {
                int extractedInt;
                string extractedString;

                if (int.TryParse(line, out extractedInt))
                {
                    pdfData.index = extractedInt;
                }
                else
                {
                    // Value is a string
                    extractedString = line;
                    pdfData.text = extractedString;
                }
            }
        }

        // Close the PDF document
        //document.Close();

        //// Save the ScriptableObject asset to a file
        string path = "Assets/PDFData.asset";
        UnityEditor.AssetDatabase.CreateAsset(pdfData, path);
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();

        return pdfData;
    }
}
