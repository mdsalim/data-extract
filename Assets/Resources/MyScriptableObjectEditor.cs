using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Content;
using PdfSharp.Pdf.Content.Objects;
using System;

[CustomEditor(typeof(DataGenerator))]
public class MyScriptableObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DataGenerator myObject = (DataGenerator)target;

        if (GUILayout.Button("Convert to XML"))
        {
            ConvertToXml(myObject.myScriptableObject);
        }
        if (GUILayout.Button("XML to ScriptableObject"))
        {
            XmlToScriptableObject("/myObjecter.xml");
        }
        if (GUILayout.Button("Convert To Pdf"))
        {
            ConvertToXmlAndPdf(myObject.myScriptableObject);
        }
        if (GUILayout.Button("Pdf To ScriptableObject"))
        {
            PdfToScriptableObject();
        }
    }
    private void ConvertToXml(MyScriptableObject scriptableObject)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(MyScriptableObject));
        string filePath = Application.dataPath + "/myObjecter.xml"; // Path to save the XML file

        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
        {
            serializer.Serialize(fileStream, scriptableObject);
        }

        Debug.Log("ScriptableObject converted to XML and saved as " + filePath);

        string pdfFilePath = Application.dataPath + "/myXMLObject.pdf";

        // Create a new PDF document
        PdfDocument document = new PdfDocument();
        PdfPage page = document.AddPage();

        XGraphics gfx = XGraphics.FromPdfPage(page);
        XFont textfont = new XFont("arial", 12, XFontStyle.Bold);
       
        // Read the XML content
        string xmlContent = File.ReadAllText(filePath);
        XTextFormatter textFormatter = new XTextFormatter(gfx);

        // Draw the XML content on the PDF page
        textFormatter.DrawString(xmlContent, textfont, XBrushes.Black, new XRect(10, 10, page.Width - 20, page.Height - 20), XStringFormats.TopLeft);

        // Save the PDF document
        document.Save(pdfFilePath);
        document.Close();

        Debug.Log("ScriptableObject converted to PDF and saved as " + pdfFilePath);

    }

    private void XmlToScriptableObject(string xmlFilePath)
    {
        MyScriptableObject importedData = XMLParser.ParseXML(xmlFilePath);

        // Specify the complete asset path including the filename and extension
        string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(xmlFilePath) + ".asset");

        UnityEditor.AssetDatabase.CreateAsset(importedData, assetPath);

        Debug.Log("XML data imported successfully!");

        string pdfFilePath = Application.dataPath + "/MyScritableObjecter.pdf";

        // Create a new PDF document
        PdfDocument document = new PdfDocument();
        PdfPage page = document.AddPage();

        XGraphics gfx = XGraphics.FromPdfPage(page);
        XFont textfont = new XFont("arial", 6, XFontStyle.Regular);

        // Read the XML content
        string xmlContent = File.ReadAllText(assetPath);
        XTextFormatter textFormatter = new XTextFormatter(gfx);

        // Draw the XML content on the PDF page
        textFormatter.DrawString(xmlContent, textfont, XBrushes.Black, new XRect(15, 15, page.Width - 20, page.Height - 20), XStringFormats.TopLeft);

        // Save the PDF document
        document.Save(pdfFilePath);
        document.Close();

        Debug.Log("XML To ScriptableObject converted to PDF and saved as " + pdfFilePath);
    }

    private void ConvertToXmlAndPdf(MyScriptableObject scriptableObject)
    {
        // Convert ScriptableObject to XML
        XmlSerializer serializer = new XmlSerializer(typeof(MyScriptableObject));
        string xmlFilePath = Application.dataPath + "/ScriptableObject_To_XML.xml";

        using (FileStream fileStream = new FileStream(xmlFilePath, FileMode.Create))
        {
            serializer.Serialize(fileStream, scriptableObject);
        }

        Debug.Log("ScriptableObject converted to XML and saved as " + xmlFilePath);

        // Convert XML to PDF
        string pdfFilePath = Application.dataPath + "/XML_To_PDF.pdf";

        // Create a new PDF document
        PdfDocument document = new PdfDocument();
        PdfPage page = document.AddPage();

        XGraphics gfx = XGraphics.FromPdfPage(page);
        XFont font = new XFont("Arial", 12, XFontStyle.Regular);

        // Read the XML content
        string xmlContent = File.ReadAllText(xmlFilePath);

        XRect textRect = new XRect(10, 10, page.Width - 20, page.Height - 20);

        // Create a text formatter for wrapping lines
        XTextFormatter textFormatter = new XTextFormatter(gfx);

        // Format and draw the XML content on the PDF page
        textFormatter.DrawString(xmlContent, font, XBrushes.Black, textRect, XStringFormats.TopLeft);

        // Save the PDF document
        document.Save(pdfFilePath);
        document.Close();

        Debug.Log("ScriptableObject converted to PDF and saved as " + pdfFilePath);
    }

    private void PdfToScriptableObject()
    {
        string PdfPath = Application.dataPath + "/MyScritableObjecter.pdf";
        PdfDocument document = PdfReader.Open(PdfPath, PdfDocumentOpenMode.ReadOnly);

        // Extract the contents of the PDF
        string pdfContents = ExtractPdfContents(document);

        // Split the contents to retrieve the values
        string delimiter = "delimiter";
        string[] splitContents = pdfContents.Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

        // Create a new instance of your ScriptableObject
        MyScriptableObject scriptableObject = ScriptableObject.CreateInstance<MyScriptableObject>();

        // Set the values in the ScriptableObject variables
        if (splitContents.Length >= 2)
        {
            int indexValue;
            if (int.TryParse(splitContents[0], out indexValue))
            {
                scriptableObject.index = indexValue;
            }

            scriptableObject.text = splitContents[1];
        }

        //// Set the PDF contents to the ScriptableObject
        //scriptableObject.SetPdfContents(pdfContents.Trim());

        // Optionally, you can save the ScriptableObject as an asset in your project
        string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(PdfPath) + ".asset");

        // UnityEditor.AssetDatabase.CreateAsset(scriptableObject, "Assets/Path/To/Save/ScriptableObject.asset");
        UnityEditor.AssetDatabase.CreateAsset(scriptableObject, assetPath);

        Debug.Log("Pdf data imported successfully!");
    }

    private string ExtractPdfContents(PdfDocument document)
    {
        StringBuilder sb = new StringBuilder();

        //var contents = ContentReader.ReadContent(document.AddPage());
        //sb.Append(contents);

        foreach (PdfPage page in document.Pages)
        {
            //var content = ContentReader.ReadContent(page);
            var content = ContentReader.ReadContent(page);
            sb.Append(content);
        }
        return sb.ToString();
    }
}
